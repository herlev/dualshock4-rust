#![cfg_attr(feature = "clippy", feature(plugin))]
#![cfg_attr(feature = "clippy", plugin(clippy))]

mod dualshock4;

pub use dualshock4::{
  get_device, get_device_old, read, AnalogStick, AnalogSticks, Button, Buttons, Dualshock4Data, Headset,
};
